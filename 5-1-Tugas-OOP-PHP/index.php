<?php

trait Hewan{
	public $nama;
	public $darah = 50;
	public $jumlahKaki;
	public $keahlian;

	public function __construct($nama ,$darah ,$jumlahKaki ,$keahlian) {
    $this->nama = $nama;
    $this->darah = $darah;
    $this->jumlahKaki = $jumlahKaki;
    $this->keahlian = $keahlian;
	}

	public function atraksi()
	{
		echo "$this->nama Sedang .$this->keahlian";
	}
}

trait Fight{
	public $attackPower;
	public $defencePower;

	public function __construct($attackPower,$defencePower){
		$this->attackPower = $attackPower;
		$this->defencePower = $defencePower;
	}
	
	public function serang()
	{
		echo "$this->attackPower menyerang .$this->defencePower";
	}
	public function diserang()
	{
		echo "$this->defencePower diserang .$this->attackPower"
	}
}


class Elang
{
	use Hewan;

	public function getInfoHewa()
	{
		use Hewan;
		use Fight;

	}
}

class Harimau
{
	use Hewan;

	public function getInfoHewa()
	{
		use Hewan;
		use Fight;
	}
}


$harimau1 = new Harimau('Harimau1',50,4,'Lari Cepat');
$harimau1->Hewan();
echo "<br>";
$elang1 = new Elang('Elang',50,2,'Terbang tinggi');
$elang1->Hewan();
echo "<br>";

?>


