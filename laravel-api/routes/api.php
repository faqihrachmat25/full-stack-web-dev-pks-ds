<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/test', function(){
	return 'Web Service Pertama';
});

Route::apiResource('/post', 'PostController@index');
// Route::apiResource('/comment', 'CommentController');

// Route::group([
// 	'prefix' => 'auth',
// 	'namespace' => 'Auth'
// ] ,function(){
// 	Route::post('register','RegisterController')->name('register.auth');
// });

