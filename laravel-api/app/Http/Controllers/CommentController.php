<?php

namespace App\Http\Controllers;

// use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{    
    // /**
    //  * index
    //  *
    //  * @return void
    //  */
    // public function index()
    // {
    //     //get data from table posts
    //     $commets = Comment::latest()->get();

    //     //make response JSON
    //     return response()->json([
    //         'success' => true,
    //         'message' => 'List Data Comment',
    //         'data'    => $posts  
    //     ], 200);

    // }
    
    //  /**
    //  * show
    //  *
    //  * @param  mixed $id
    //  * @return void
    //  */
    // public function show($id)
    // {
    //     //find post by ID
    //     $commets = Comment::findOrfail($id);

    //     //make response JSON
    //     return response()->json([
    //         'success' => true,
    //         'message' => 'Detail Data Commet',
    //         'data'    => $post 
    //     ], 200);

    // }
    
    // /**
    //  * store
    //  *
    //  * @param  mixed $request
    //  * @return void
    //  */
    // public function store(Request $request)
    // {
    //     //set validation
    //     $validator = Validator::make($request->all(), [
    //         'comment'	,
            
    //         'post_id' 	=> 'required'
    //     ]);
        
    //     //response error validation
    //     if ($validator->fails()) {
    //         return response()->json($validator->errors(), 400);
    //     }

    //     //save to database
    //     $comment = Comment::create([
    //         'comment' 	=> $request->comment,
            
    //         'post_id' 	=> $request->post_id
    //     ]);

    //     //success save to database
    //     if($comment) {

    //         return response()->json([
    //             'success' => true,
    //             'message' => 'Comment Created',
    //             'data'    => $comment  
    //         ], 201);

    //     } 

    //     //failed save to database
    //     return response()->json([
    //         'success' => false,
    //         'message' => 'Comment Failed to Save',
    //     ], 409);

    // }
    
    // /**
    //  * update
    //  *
    //  * @param  mixed $request
    //  * @param  mixed $post
    //  * @return void
    //  */
    // public function update(Request $request, Comment $comment)
    // {
    //     //set validation
    //     $validator = Validator::make($request->all(), [
    //         'title'   => 'required',
    //         'description' => 'required',
    //     ]);
        
    //     //response error validation
    //     if ($validator->fails()) {
    //         return response()->json($validator->errors(), 400);
    //     }

    //     //find post by ID
    //     $comment = Comment::findOrFail($comment->id);

    //     if($post) {

    //         //update post
    //         $comment->update([
    //         	'comment' 	=> $request->comment,
            	
    //         	'post_id' 	=> $request->post_id
    //         ]);

    //         return response()->json([
    //             'success' => true,
    //             'message' => 'Comment Updated',
    //             'data'    => $comment  
    //         ], 200);

    //     }

    //     //data post not found
    //     return response()->json([
    //         'success' => false,
    //         'message' => 'Comment Not Found',
    //     ], 404);

    // }
    
    // /**
    //  * destroy
    //  *
    //  * @param  mixed $id
    //  * @return void
    //  */
    // public function destroy($id)
    // {
    //     //find post by ID
    //     $comment = Comment::findOrfail($id);

    //     if($post) {

    //         //delete post
    //         $post->delete();

    //         return response()->json([
    //             'success' => true,
    //             'message' => 'Comment Deleted',
    //         ], 200);

    //     }

    //     //data post not found
    //     return response()->json([
    //         'success' => false,
    //         'message' => 'Comment Not Found',
    //     ], 404);
    // }
}