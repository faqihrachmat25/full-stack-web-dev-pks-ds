<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class OtpCode extends Model
{
    protected $fillable = ['id','otp','user_id'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot()
    {
    	parent::boot();

    	static::creating(function ($model)
    	{
    		if(	empty($model -> {$model->getKeyName()}))
    		{
    			$model -> {$model -> getKeyName()} = Str::uuid();
    		}
    	});
    }

    public function User()
    {
        return $this->belongsTo('App\User');
    }
}
